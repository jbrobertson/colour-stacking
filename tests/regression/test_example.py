from colour_stacking.input import read_geojson
from colour_stacking.solver import solve


def test_example():
    input_config, pc = read_geojson("tests/data/example.geojson")
    result = solve(pc, input_config)

    assert result == [
        {"polygon_index": 0, "colour": "blue", "thickness": 2},
        {"polygon_index": 0, "colour": "red", "thickness": 3},
        {"polygon_index": 1, "colour": "blue", "thickness": 2},
        {"polygon_index": 2, "colour": "orange", "thickness": 1}
    ]
