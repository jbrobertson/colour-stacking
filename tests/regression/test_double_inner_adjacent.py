import shapely.wkt as wkt

from colour_stacking.input_config import InputConfig
from colour_stacking.solver import solve


def test_inner_adjacent():
    colours = ["red", "blue", "yellow", "purple", "pink", "orange"]

    polygon_1 = wkt.loads("POLYGON((0 0, 10 0, 10 10, 0 10, 0 0))")
    polygon_2 = wkt.loads("POLYGON((0 0, 1 0, 1 1, 0 1, 0 0))")

    polygons_and_colours = [
        (polygon_1, ["blue", "orange"]),
        (polygon_2, ["yellow"]),
    ]

    input_config = InputConfig(buffer_size=0.1, colour_order=colours)
    result = solve(polygons_and_colours, input_config)

    assert result == [
        {'polygon_index': 0, 'colour': 'blue', 'thickness': 1},
        {'polygon_index': 0, 'colour': 'orange', 'thickness': 3},
        {'polygon_index': 1, 'colour': 'yellow', 'thickness': 2}
    ]
