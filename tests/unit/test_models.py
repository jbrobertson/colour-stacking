import shapely.wkt as wkt
import json
from colour_stacking.models import GeoJsonInput


def test_geojson_model():
    d = json.load(open("tests/data/example.geojson"))
    model = GeoJsonInput(**d)
    assert model.properties.ordered_colours == [
        "orange", "blue", "yellow",
        "green", "purple", "pink", "red"]
    assert model.properties.buffer_size == 0.1
    assert len(model.features) == 3
    assert model.features[0].properties.colours == ['blue', 'red']
    assert model.features[0].geometry == wkt.loads(
        "POLYGON((0 0, 10 0, 10 10, 0 10, 0 0))"
    )

    assert model.features[1].properties.colours == ["blue"]
    assert model.features[1].geometry == wkt.loads(
        "POLYGON((6 0, 7 0, 7 7, 6 7, 6 0))"
    )

    assert model.features[2].properties.colours == ["orange"]
    assert model.features[2].geometry == wkt.loads(
        "POLYGON((0 0, 6 0, 6 3, 0 3, 0 0))"
    )
