import shapely.wkt as wkt
from colour_stacking.input import read_geojson


def test_input():
    input_config, polygons_and_colours = read_geojson("tests/data/example.geojson")
    assert input_config.colour_order == [
        "orange", "blue", "yellow",
        "green", "purple", "pink", "red"]
    assert input_config.buffer_size == 0.1
    assert input_config.opposite_share_thickness is False
    assert len(polygons_and_colours) == 3

    assert polygons_and_colours[0][1] == ['blue', 'red']
    assert polygons_and_colours[0][0] == wkt.loads(
        "POLYGON((0 0, 10 0, 10 10, 0 10, 0 0))"
    )

    assert polygons_and_colours[1][1] == ["blue"]
    assert polygons_and_colours[1][0] == wkt.loads(
        "POLYGON((6 0, 7 0, 7 7, 6 7, 6 0))"
    )

    assert polygons_and_colours[2][1] == ["orange"]
    assert polygons_and_colours[2][0] == wkt.loads(
        "POLYGON((0 0, 6 0, 6 3, 0 3, 0 0))"
    )
